# Import randint from random
from random import randint

# Declares a list of months
months = ["January", "February", "March", "April",
            "May", "June", "July", "August", "September",
            "October", "November", "December"]

# Declares a list of days for each month
days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

min_year = 1924
max_year = 2004

# Ask user for name with prompt
name = input("Hi! What is your name? ")

for guess_number in range(1, 6):

    # Variable storing the possible years
    guess_year = randint(min_year, max_year)

    # Variable storing the possible months
    guess_month = randint(1, 12)

    # Variable storing the possible day
    guess_day = days[guess_month - 1]

    # Print our prompt for guess
    print(f'Guess {guess_number} : {name} were you born in {months[guess_month - 1]} / {guess_day} / {guess_year} ?')

    # Ends the game on the fifth try
    if guess_number == 5:
        response = input("yes or no? ")
        print("I have other things to do. Good bye.")
        exit()

    # Reponses to the user for the first four guesses
    response = input("yes, earlier, or later? ")
    if response == "yes":
        print("I knew it!")
        exit()
    elif response == "earlier":
        if guess_year == 1924:
            print("The year can't go any lower!")
        print("Drat! Lemme try again!")
        max_year = guess_year
    else:
        if guess_year == 2004:
            print("The year can't go any higher!")
        print("Drat! Lemme try again!")
        min_year = guess_year
